import React from 'react';
import Map from 'pigeon-maps';
import Marker from 'pigeon-marker';
import { BASE_URL } from './Endpoints.js';
import './App.css';

const defaultCenter = [ 18.251021795371233, 94.1601184007389 ];
const defaultZoom = 3;
const defaultBounds = {
  "ne": [
    62.5633721180814,
    147.1054330465061
  ],
  "sw": [
    -39.82476468628612,
    41.03902250497168
  ]
};

var gettingAddtionalPictures = false;

class App extends React.Component {
  constructor(props) {
    super(props);

    this.getPictures = this.getPictures.bind(this);
    this.handleNextButton = this.handleNextButton.bind(this);
    this.setCurrentPicture = this.setCurrentPicture.bind(this);
    this.goToPicture = this.goToPicture.bind(this);

    this.state = {
      photoList: [],
      center: defaultCenter,
      zoom: defaultZoom,
      bounds: defaultBounds,
      loading: true,
      currentPage: 0,
      totalPages: 0,
      currentPicture: ""
    }
  }

  handleBoundsChange = async ({ center, zoom, bounds, initial }) => {
    this.setState({
      center: center,
      zoom: zoom,
      bounds: bounds,
      currentPage: 0,
      totalPages: 0,
      currentPicture: ""
    });

    this.getPictures(bounds, true);
  }

  handleNextButton = async (next) => {
    this.getPictures(this.state.bounds, next);
  }

  async getPictures(bounds, isNext) {
    //console.log(bounds);
    this.setState({
      loading: true,
      photoList: [],
      currentPicture: ""
    });

    setTimeout(async () => {
      const currBounds = this.state.bounds;

      if (bounds.sw[0] === currBounds.sw[0] && bounds.sw[1] === currBounds.sw[1] && bounds.ne[0] === currBounds.ne[0] && bounds.ne[1] === currBounds.ne[1]) {
        console.log("getting pictures for: ");
        console.log(bounds);

        const method = "flickr.photos.search";
        const bbox = `${bounds.sw[1]},${bounds.sw[0]},${bounds.ne[1]},${bounds.ne[0]}`;
        const extras = "geo,date_taken,owner_name";
        const per_page = "30";
        const page = isNext ? this.state.currentPage + 1 : this.state.currentPage - 1;
        const sort = "interestingness-desc";

        //get pictures that were taken less than 5 years ago
        var date = new Date();
        date.setFullYear(date.getFullYear() - 5);
      
        const min_taken_date = Math.round(date.getTime() / 1000);

        const queryURL = `${BASE_URL}/flickrapi/?method=${method}&min_taken_date=${min_taken_date}&sort=${sort}&bbox=${bbox}&extras=${extras}&per_page=${per_page}&page=${page}&format=json&nojsoncallback=1`;

        try {
          const response = await fetch(queryURL);
          const data = await response.json();
          const newPhotos = data.photos.photo;

          //check again in case user has updated the bounds
          const currBoundsNew = this.state.bounds;

          if (bounds.sw[0] === currBoundsNew.sw[0] && bounds.sw[1] === currBoundsNew.sw[1] && bounds.ne[0] === currBoundsNew.ne[0] && bounds.ne[1] === currBoundsNew.ne[1]) {  
            this.setState({
                photoList: newPhotos,
                loading: false,
                currentPage: page,
                totalPages: data.photos.pages
            });

            gettingAddtionalPictures = false;
          }
        } catch (err) {
          console.log(err);
        }
      }
    }, 2000);
  }

  setCurrentPicture = (id = "") => {
    this.setState({
      currentPicture: id
    });
  }

  goToPicture = (id) => {
    document.getElementById(id).scrollIntoView();
  }

  render() {
    const markers = this.state.photoList.map(data => {
      return (
        <Marker key={data.id} important={this.state.currentPicture === data.id} anchor={[Number(data.latitude), Number(data.longitude)]} onClick={() => this.goToPicture(data.id)}/>
      );
    });

    return (
      <div className="App" style={{ display: "flex", flexDirection: "column" }}>
        <div style={{ backgroundColor: "#F5F5F5", height: "70px", width: "100%" }}>
          <h2 style={{ marginLeft: "30px" }}>Pictures - a location based Flickr browser</h2>
        </div>

        <div className="content" style={{ flex: "1", overflow: "hidden" }}>
          <div style={{ width: "40%", height: "100%", float: "left" }}>
            <Map
              defaultCenter={defaultCenter}
              defaultZoom={defaultZoom}
              onBoundsChanged={this.handleBoundsChange}>
              {markers}
            </Map>            
          </div>
          <div style={{overflowY: "scroll", width: "60%", height: "94%", float: "left"}}>
            <PhotoList
              style={{ columnCount: "2", columnGap: "5px"}}
              photos={this.state.photoList}
              loading={this.state.loading}
              getPictures={this.getPictures}
              bounds={this.state.bounds}
              setCurrentPicture={this.setCurrentPicture}
            />
          </div>
          <div style={{ width: "60%", height: "6%", float: "left", textAlign: "center"}}>
            <div style={{marginTop: "5px", fontSize: "16px"}}>
              <button style={{width: "100px", height: "30px", fontSize: "16px"}} onClick={() => this.handleNextButton(false)} disabled={this.state.loading || this.state.currentPage === 1}>Previous</button>
              <span style={{marginLeft: "20px", marginRight: "20px"}}>{this.state.currentPage === 0 ? "" : "Page " + this.state.currentPage + " / " + this.state.totalPages} </span>
              <button style={{width: "100px", height: "30px", fontSize: "16px"}} onClick={() => this.handleNextButton(true)} disabled={this.state.loading || this.state.currentPage >= this.state.totalPages}>Next</button>
            </div>
          </div>
        </div>

        <div style={{ backgroundColor: "#F5F5F5", height: "50px", width: "100%", overflow: "hidden", clear: "both" }}>
          <p style={{ marginLeft: "30px" }}>pictures.jqstuff.com |&nbsp;
          <a href="https://bitbucket.org/happymonkey12/pictures/src/master/" target="_blank" rel="noopener noreferrer">Source Code</a> | 
          Picture Source: <a href="https://www.flickr.com/services/api/" target="_blank" rel="noopener noreferrer">Flickr API</a> | 
          Map: <a href="https://github.com/mariusandra/pigeon-maps" target="_blank" rel="noopener noreferrer">Pigeon Maps</a> |
          All photos are the property of their respective authors.
          </p>
        </div>
      </div>
    );
  }
}

class PhotoList extends React.Component {
  constructor(props) {
    super(props);
    this.handleScroll = this.handleScroll.bind(this);
  }

  handleScroll = (e) => {
    const more = e.target.scrollHeight - e.target.scrollTop < 5 * e.target.clientHeight;
    if (!gettingAddtionalPictures && more) {
      gettingAddtionalPictures = true;
      this.props.getPictures(this.props.bounds, true);
    }
  }

  render() {
    if (this.props.loading === true || this.props.photos.length === 0) {
      const message = this.props.loading ? "Loading pictures ..." : "No pictures found for selected area";
      return (
        <div style={{textAlign: "center", lineHeight: "350px", fontSize: "38px"}}>
          {message}
      </div>
      );
    }

    const photoList = this.props.photos.map(data => {
      const photoUrl = `https://farm${data.farm}.staticflickr.com/${data.server}/${data.id}_${data.secret}.jpg`;
      const onClick = `https://www.flickr.com/photos/${data.owner}/${data.id}`;
      const date = data.datetaken.split(" ")[0];

      return (
        <Photo setCurrentPicture={this.props.setCurrentPicture} key={data.id} id={data.id} url={photoUrl} onClick={onClick} title={data.title} owner={data.ownername} date={date}/>
      );
    }); 

    return (
     <div style={this.props.style}>
       {photoList}
   </div>
    );
  }
}

class Photo extends React.Component {
  constructor(props) {
    super(props);

    this.handleOnMouseOver = this.handleOnMouseOver.bind(this);
    this.handleOnMouseOut = this.handleOnMouseOut.bind(this);
  }

  handleOnMouseOver = () => {
    this.props.setCurrentPicture(this.props.id);
  }

  handleOnMouseOut = () => {
    this.props.setCurrentPicture();
  }

  render() {
    return (
      <div style={{ width: "100%", marginBottom: "20px"}} id={this.props.id} onMouseOver={this.handleOnMouseOver} onMouseOut={this.handleOnMouseOut} onClick={() => window.open(this.props.onClick, "_blank")}>
        <div style={{padding: "10px", backgroundColor: "#EEEEEE", textAlign: "left"}}>
          <div style={{fontWeight: "bold"}}>{this.props.title}</div>
          <div style={{fontSize: "13px"}}>{this.props.owner} @ {this.props.date}</div>
        </div>
        <img src={this.props.url} alt="" width="100%" />
      </div>
    );
  }
}

export default App;
