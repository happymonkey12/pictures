var express = require('express');
var router = express.Router();
const fetch = require("node-fetch");
require('dotenv').config();
const api_key = process.env.API_KEY;

router.get('/flickrapi', async function(req, res, next) {
  const method = req.query.method;
  const bbox = req.query.bbox;
  const extras = req.query.extras;
  const per_page = req.query.per_page;
  const page = req.query.page;
  const sort = req.query.sort;
  const min_taken_date = req.query.min_taken_date;

  const queryURL = `https://www.flickr.com/services/rest/?method=${method}&min_taken_date=${min_taken_date}&sort=${sort}&api_key=${api_key}&bbox=${bbox}&extras=${extras}&per_page=${per_page}&page=${page}&format=json&nojsoncallback=1`;

  //console.log("--------");
  //console.log(queryURL);

  try {
    const response = await fetch(queryURL);
    const data = await response.json();
    res.json(data);
  } catch (err) {
    console.log(err);
    res.json({
      status: "error"
    });
  }
});

module.exports = router;

/*
`https://www.flickr.com/services/rest/?method=${method}&api_key=${api_key}&bbox=${bbox}&extras=${extras}&per_page=${per_page}&page=${page}&format=json&nojsoncallback=1`;
*/